-- FL2016
-- Select only labelled reviews, and replace all instances of "not [word]" with "![word]" for negation handling
DROP VIEW IF EXISTS labelled_only;
CREATE VIEW labelled_only AS
SELECT hw_id, hw_number, label, REGEXP_REPLACE(review, 'not\\s+', '!') AS review
	FROM fl2016 
	WHERE label IN ('positive', 'negative');

-- Break up reviews into individual words
DROP VIEW IF EXISTS splitWords;
CREATE VIEW splitWords AS
SELECT hw_id, hw_number, label, word 
	FROM labelled_only
        LATERAL VIEW explode(split(review, ' ')) splitWords as word;

-- Identify if words have sentiment value
DROP VIEW IF EXISTS word_sents;
CREATE VIEW word_sents AS
SELECT r.hw_id, r.hw_number, r.label, r.word, sentiment_words.sentiment AS sentiment
	FROM splitWords r
	LEFT OUTER JOIN sentiment_words
	ON (r.word = sentiment_words.word);

-- Count number of positive and negative words per review, and total words
DROP VIEW IF EXISTS sent_counts;
CREATE VIEW sent_counts AS
SELECT hw_id, hw_number, label, COUNT(*) as numWords, COUNT(case sentiment when 'positive' then 1 else null end) AS numPos, COUNT(CASE sentiment WHEN 'negative' THEN 1 ELSE NULL end) AS numNeg
	FROM word_sents
	GROUP BY hw_id, hw_number, label;

-- calculate the sentiment score.
DROP VIEW IF EXISTS stats;
CREATE VIEW stats AS
SELECT hw_id, hw_number, label, (numPos - numNeg)/(numPos +  numNeg) AS sentiment_score
	FROM sent_counts;

-- Predict a sentiment based on the stat in question, and store in a table
DROP TABLE IF EXISTS fl2016_predictions;
CREATE TABLE fl2016_predictions AS
SELECT hw_id, hw_number, label, sentiment_score,
	CASE WHEN sentiment_score<0 THEN 'negative' 
	ELSE 'positive' END AS prediction
	FROM stats;

-- SP2017

DROP VIEW labelled_only;
CREATE VIEW labelled_only AS
SELECT hw_id, hw_number, label, REGEXP_REPLACE(review, 'not\\s+', '!') AS review
        FROM sp2017
        WHERE label IN ('positive', 'negative');

DROP TABLE IF EXISTS sp2017_predictions;
CREATE TABLE sp2017_predictions AS
SELECT hw_id, hw_number, label, sentiment_score,
        CASE WHEN sentiment_score<0 THEN 'negative'
        ELSE 'positive' END AS prediction
        FROM stats;

-- FL2017

DROP VIEW labelled_only;
CREATE VIEW labelled_only AS
SELECT hw_id, hw_number, label, REGEXP_REPLACE(review, 'not\\s+', '!') AS review
        FROM fl2017
        WHERE label IN ('positive', 'negative');

DROP TABLE IF EXISTS fl2017_predictions;
CREATE TABLE fl2017_predictions AS
SELECT hw_id, hw_number, label, sentiment_score,
        CASE WHEN sentiment_score<0 THEN 'negative'
        ELSE 'positive' END AS prediction
        FROM stats;

-- SP2018

DROP VIEW labelled_only;
CREATE VIEW labelled_only AS
SELECT hw_id, hw_number, label, REGEXP_REPLACE(review, 'not\\s+', '!') AS review
        FROM sp2018
        WHERE label IN ('positive', 'negative');

DROP TABLE IF EXISTS sp2018_predictions;
CREATE TABLE sp2018_predictions AS
SELECT hw_id, hw_number, label, sentiment_score,
        CASE WHEN sentiment_score<0 THEN 'negative'
        ELSE 'positive' END AS prediction
        FROM stats;

-- FL2018

DROP VIEW labelled_only;
CREATE VIEW labelled_only AS
SELECT hw_id, hw_number, label, REGEXP_REPLACE(review, 'not\\s+', '!') AS review
        FROM fl2018
        WHERE label IN ('positive', 'negative');

DROP TABLE IF EXISTS fl2018_predictions;
CREATE TABLE fl2018_predictions AS
SELECT hw_id, hw_number, label, sentiment_score,
        CASE WHEN sentiment_score<0 THEN 'negative'
        ELSE 'positive' END AS prediction
        FROM stats;


-- example for finding the percentage correctly predicted for a particular predictions table:
--SELECT COUNT(IF(label=prediction, 'correct', NULL))/COUNT(*)
--	FROM fl2016_predictions;
