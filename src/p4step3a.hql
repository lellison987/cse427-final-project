-- This script analyzes the average per month (so group of 3 hw assignments) both for every year, and as a total

-- First we first re-label each entry so that 1,2,3 become 1-3, 4,5,6 become 4-6, and 7,8,9 become 7-9 so that we can group them later
-- Then	we find the average sentiment score for each of these new month labels, repeating for each year	

-- Sp 17
DROP VIEW IF EXISTS sp17_monthly;
CREATE VIEW sp17_monthly AS
        SELECT CASE WHEN '1, 2, 3' rlike hw_number then '1-3'
	WHEN '4, 5, 6' rlike hw_number THEN '4-6'
	ELSE '7-9' END AS hw_number,
	sentiment_score
        FROM sp2017_predictions;

DROP VIEW IF EXISTS sp17_monthly_sent;
CREATE VIEW sp17_monthly_sent AS
	SELECT 'sp2017' as year, hw_number, AVG(sentiment_score) AS sentiment_score
	FROM sp17_monthly
	GROUP BY hw_number;

-- Sp 18
DROP VIEW IF EXISTS sp18_monthly;
CREATE VIEW sp18_monthly AS
        SELECT CASE WHEN '1, 2, 3' rlike hw_number then '1-3'
        WHEN '4, 5, 6' rlike hw_number THEN '4-6'
        ELSE '7-9' END AS hw_number,
        sentiment_score
        FROM sp2018_predictions;

DROP VIEW IF EXISTS sp18_monthly_sent;
CREATE VIEW sp18_monthly_sent AS
        SELECT 'sp2018' AS year, hw_number, AVG(sentiment_score) AS sentiment_score
        FROM sp18_monthly
        GROUP BY hw_number;

-- Fl 16
DROP VIEW IF EXISTS fl16_monthly;
CREATE VIEW fl16_monthly AS
        SELECT CASE WHEN '1, 2, 3' rlike hw_number then '1-3'
        WHEN '4, 5, 6' rlike hw_number THEN '4-6'
        ELSE '7-9' END AS hw_number,
        sentiment_score
        FROM fl2016_predictions;

DROP VIEW IF EXISTS fl16_monthly_sent;
CREATE VIEW fl16_monthly_sent AS
        SELECT 'fl2016' AS year, hw_number, AVG(sentiment_score) AS sentiment_score
        FROM fl16_monthly
        GROUP BY hw_number;

-- Fl 17
DROP VIEW IF EXISTS fl17_monthly;
CREATE VIEW fl17_monthly AS
        SELECT CASE WHEN '1, 2, 3' rlike hw_number then '1-3'
        WHEN '4, 5, 6' rlike hw_number THEN '4-6'
        ELSE '7-9' END AS hw_number,
        sentiment_score
        FROM fl2017_predictions;

DROP VIEW IF EXISTS fl17_monthly_sent;
CREATE VIEW fl17_monthly_sent AS
        SELECT 'fl2017' AS year, hw_number, AVG(sentiment_score) AS sentiment_score
        FROM fl17_monthly
        GROUP BY hw_number;

-- Fl 18
DROP VIEW IF EXISTS fl18_monthly;
CREATE VIEW fl18_monthly AS
        SELECT CASE WHEN '1, 2, 3' rlike hw_number then '1-3'
        WHEN '4, 5, 6' rlike hw_number THEN '4-6'
        ELSE '7-9' END AS hw_number,
        sentiment_score
        FROM fl2018_predictions;

DROP VIEW IF EXISTS fl18_monthly_sent;
CREATE VIEW fl18_monthly_sent AS
        SELECT 'fl2018' AS year, hw_number, AVG(sentiment_score) AS sentiment_score
        FROM fl18_monthly
        GROUP BY hw_number;

-- Now concatinate all of the tables together into one unified table
DROP TABLE IF EXISTS all_reviews;
CREATE TABLE all_reviews AS
        SELECT year, hw_number, sentiment_score
        FROM sp17_monthly_sent
        UNION ALL
        SELECT year, hw_number, sentiment_score
        FROM sp18_monthly_sent
        UNION ALL
        SELECT year, hw_number, sentiment_score 
        FROM fl16_monthly_sent
        UNION ALL
        SELECT year, hw_number, sentiment_score 
        FROM fl17_monthly_sent
        UNION ALL
        SELECT year, hw_number, sentiment_score
	FROM fl18_monthly_sent;

-- Compute the overal monthly average
DROP TABLE IF EXISTS concat_month_avg
CREATE TABLE concat_month_avg AS
	SELECT hw_number, AVG(sentiment_score) AS sentiment_score
	FROM all_reviews
	GROUP BY hw_number;

DROP VIEW sp17_monthly;
DROP VIEW sp18_monthly;
DROP VIEW fl16_monthly;
DROP VIEW fl17_monthly;
DROP VIEW fl18_monthly;
DROP VIEW sp17_monthly_sent;
DROP VIEW sp18_monthly_sent;
DROP VIEW fl16_monthly_sent;
DROP VIEW fl17_monthly_sent;
DROP VIEW fl18_monthly_sent;

