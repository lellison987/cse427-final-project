-- step 1a)
DROP VIEW IF EXISTS all_reviews;
CREATE VIEW all_reviews AS
SELECT * FROM fl2016
	UNION ALL SELECT * FROM sp2017
	UNION ALL SELECT * FROM fl2017
	UNION ALL SELECT * FROM sp2018
	UNION ALL SELECT * FROM fl2018;

DROP VIEW IF EXISTS split_words;
CREATE VIEW split_words AS
SELECT hw_id, hw_number, label, word
	FROM all_reviews
	LATERAL VIEW EXPLODE(SPLIT(review, ' ')) split_words AS word;

-- attach a sentiment to sentimented words
DROP TABLE IF EXISTS wordSents;
CREATE TABLE wordSents AS
SELECT r.hw_id, r.hw_number, r.label, r.word, sentiment_words.sentiment AS sentiment
        FROM fl2016_splitWords r
        LEFT OUTER JOIN sentiment_words
        ON (r.word = sentiment_words.word);

-- find top 5 most frequent positive words
SELECT word, COUNT(*) AS frequency  FROM wordSents 
	WHERE sentiment='positive'
	GROUP BY word 
	ORDER BY frequency DESC
	LIMIT 5;

-- find top 5 most frequent negative words
SELECT word, COUNT(*) AS frequency  FROM wordSents
        WHERE sentiment='negative'
        GROUP BY word
        ORDER BY frequency DESC
        LIMIT 5;



-- step 1b) see p4step1b.hql

-- step 1c)
