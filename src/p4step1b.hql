-- to run this script from cmd line: 
-- hive -f p4step1b.hql --hiveconf path=~/cse427s/final-project/text/allReviews --hiveconf n=<n-gram number> 
-- make sure stop_words and sentiment_words tables already exist. If not, run the code from basic_predictor.hql.
-- creates a table of the top ngrams with frequency and sentiment score.

-- set cmd line parameters
set path;
set hiveconf:path;
set n;
set hiveconf:n;

-- create a table of all review data
DROP TABLE IF EXISTS p41b;
CREATE TABLE p41b
	(hw_id STRING,
         hw_number INT,
         label STRING,
         review STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
LOAD DATA LOCAL INPATH '${hiveconf:path}' INTO TABLE p41b;

--remove the stop words
INSERT OVERWRITE TABLE p41b
SELECT d.hw_id, d.hw_number, d.label, CONCAT_WS(" ", COLLECT_LIST(d.word)) AS
 words
        FROM (SELECT c.hw_id, c.hw_number, c.label, c.word
                FROM (SELECT b.hw_id, b.hw_number, b.label, expl.word
                        FROM (SELECT a.hw_id, a.hw_number, a.label, exp.words
                                FROM p41b a
                                LATERAL VIEW EXPLODE(SENTENCES(LOWER(a.review))) exp AS words) b
                        LATERAL VIEW EXPLODE(b.words) expl AS word) c
                LEFT OUTER JOIN stop_words s
                ON (c.word = s.word)
                WHERE s.word IS NULL) d
        GROUP BY d.hw_id, d.hw_number, d.label;


-- handle negation: turn any two words "not ___" into "not !___".
DROP VIEW IF EXISTS p41b_negations;
CREATE VIEW p41b_negations AS
SELECT REGEXP_REPLACE(review, 'not\\s+', '(negation) !') AS review
	FROM p41b;

-- generate a table of ngrams
DROP VIEW IF EXISTS ngrams;
CREATE VIEW ngrams AS
SELECT EXPLODE(NGRAMS(SENTENCES(LOWER(review)), ${hiveconf:n}, 1000)) AS ngram
	FROM p41b_negations;	

DROP VIEW IF EXISTS ngrams_columned;
CREATE VIEW ngrams_columned AS
SELECT ngram.ngram AS ngram, ngram.estfrequency AS frequency
	FROM ngrams;

-- calculate ngrams' sentiments
DROP VIEW IF EXISTS ngrams_words;
CREATE VIEW  ngrams_words AS
SELECT ngram, frequency, word
	FROM ngrams_columned
	LATERAL VIEW EXPLODE(ngram) ngrams_words AS word;

DROP VIEW IF EXISTS ngrams_sentbyword;
CREATE VIEW ngrams_sentbyword AS
SELECT ngrams_words.*, sentiment_words.sentiment AS sentiment
	FROM ngrams_words 
	LEFT OUTER JOIN sentiment_words
	ON (ngrams_words.word = sentiment_words.word);

DROP VIEW IF EXISTS pre_ngrams_sentiments;
CREATE VIEW pre_ngrams_sentiments AS
SELECT ngram, frequency, COUNT(case sentiment when 'positive' then 1 else null end) AS numPos, COUNT(CASE sentiment WHEN 'negative' THEN 1 ELSE NULL end) AS numNeg
        FROM ngrams_sentbyword
        GROUP BY ngram, frequency;

-- sum the positive and negative sentiments to get the final score
DROP TABLE IF EXISTS ngrams_sentiments;
CREATE TABLE ngrams_sentiments AS
SELECT ngram, numPos - numNeg AS sentiment, frequency
	FROM pre_ngrams_sentiments
	ORDER BY frequency DESC;

DROP TABLE p41b;
DROP VIEW p41b_negations;
DROP VIEW ngrams;
DROP VIEW ngrams_columned;
DROP VIEW ngrams_words;
DROP VIEW ngrams_sentbyword;
DROP VIEW pre_ngrams_sentiments;
