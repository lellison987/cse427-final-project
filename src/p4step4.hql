-- calculate accuracy of our algorithm on our own reviews
-- MAKE SURE tables stop_words and sentiment_words exists prior to running this script.

DROP TABLE IF EXISTS our_reviews;
CREATE TABLE our_reviews
	(author STRING,
	hw_number INT,
	label STRING,
	review STRING
	)
	ROW FORMAT DELIMITED
	FIELDS TERMINATED BY '\t';
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/ourReviews' OVERWRITE INTO TABLE our_reviews;

INSERT OVERWRITE TABLE our_reviews
SELECT d.author, d.hw_number, d.label, CONCAT_WS(" ", COLLECT_LIST(d.word)) AS
 words
        FROM (SELECT c.author, c.hw_number, c.label, c.word
                FROM (SELECT b.author, b.hw_number, b.label, expl.word
                        FROM (SELECT a.author, a.hw_number, a.label, exp.words
                                FROM our_reviews a
                                LATERAL VIEW EXPLODE(SENTENCES(LOWER(a.review))) exp AS words) b
                        LATERAL VIEW EXPLODE(b.words) expl AS word) c
                LEFT OUTER JOIN stop_words s
                ON (c.word = s.word)
                WHERE s.word IS NULL) d
        GROUP BY d.author, d.hw_number, d.label;

-- select only labelled reviews, and replace all instances of "not [word]" with "![word]" for negation handling
DROP VIEW IF EXISTS labelled_only;
CREATE VIEW labelled_only AS
SELECT author, hw_number, label, REGEXP_REPLACE(review, 'not\\s+', '!') AS review
        FROM our_reviews 
        WHERE label IN ('positive', 'negative');

-- Break up reviews into individual words
DROP VIEW IF EXISTS splitWords;
CREATE VIEW splitWords AS
SELECT author, hw_number, label, word
        FROM labelled_only
        LATERAL VIEW explode(split(review, ' ')) splitWords as word;

-- Identify if words have sentiment value
DROP VIEW IF EXISTS word_sents;
CREATE VIEW word_sents AS
SELECT r.author, r.hw_number, r.label, r.word, sentiment_words.sentiment AS sentiment
        FROM splitWords r
        LEFT OUTER JOIN sentiment_words
        ON (r.word = sentiment_words.word);

DROP VIEW IF EXISTS sent_counts;
CREATE VIEW sent_counts AS
SELECT author, hw_number, label, COUNT(*) as numWords, COUNT(case sentiment when 'positive' then 1 else null end) AS numPos, COUNT(CASE sentiment WHEN 'negative' THEN 1 ELSE NULL end) AS numNeg
        FROM word_sents
        GROUP BY author, hw_number, label;

-- calculate the sentiment score.
DROP VIEW IF EXISTS stats;
CREATE VIEW stats AS
SELECT author, hw_number, label, (numPos - numNeg)/(numPos +  numNeg) AS sentiment_score
        FROM sent_counts;

-- Predict a sentiment based on the stat in question
DROP TABLE IF EXISTS our_reviews_predictions;
CREATE TABLE our_reviews_predictions AS
SELECT author, hw_number, label, sentiment_score,
        CASE WHEN sentiment_score<0 THEN 'negative'
        ELSE 'positive' END AS prediction
        FROM stats;

-- find accuracy of predictions:
SELECT COUNT(IF(label=prediction, 'correct', NULL))/COUNT(*)
      FROM our_reviews_predictions;

