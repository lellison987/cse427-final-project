-- Atable to produce the average sentiment score for each of the homeworks for fall 18

DROP TABLE IF EXISTS fl18compare;
CREATE TABLE fl18compare AS
	SELECT hw_number, AVG(sentiment_score) AS score
	FROM fl2018_predictions
	GROUP BY hw_number;

