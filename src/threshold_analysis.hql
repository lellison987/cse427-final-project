-- To determine what metrics and thresholds to use to best separate negative, neutral and positive reviews.

-- see the mins and maxes of sentdiff, diffsum, and difftotal.
--SELECT label, MIN(sentdiff), MAX(sentdiff), MIN(diffsum), MAX(diffsum), MIN(difftotal), MAX(difftotal)
--	FROM stats
--	GROUP BY label;

-- generate histogram data for sentdiff per label
SELECT EXPLODE(HISTOGRAM_NUMERIC(sentdiff, 8)) AS sentdiff FROM stats WHERE label='negative';
SELECT EXPLODE(HISTOGRAM_NUMERIC(sentdiff, 8)) AS sentdiff FROM stats WHERE label='neutral';
SELECT EXPLODE(HISTOGRAM_NUMERIC(sentdiff, 8)) AS sentdiff FROM stats WHERE label='positive';

-- generate histogram data for diffsum per label
SELECT EXPLODE(HISTOGRAM_NUMERIC(diffsum, 8)) AS sentdiff FROM stats WHERE label='negative';
SELECT EXPLODE(HISTOGRAM_NUMERIC(diffsum, 8)) AS sentdiff FROM stats WHERE label='neutral';
SELECT EXPLODE(HISTOGRAM_NUMERIC(diffsum, 8)) AS sentdiff FROM stats WHERE label='positive';

-- generate histogram data for difftotal per label
SELECT EXPLODE(HISTOGRAM_NUMERIC(difftotal, 8)) AS sentdiff FROM stats WHERE label='negative';
SELECT EXPLODE(HISTOGRAM_NUMERIC(difftotal, 8)) AS sentdiff FROM stats WHERE label='neutral';
SELECT EXPLODE(HISTOGRAM_NUMERIC(difftotal, 8)) AS sentdiff FROM stats WHERE label='positive';
