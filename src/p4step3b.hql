-- This script computes the differences in the averages for theory and implementation as well as spark and mapreduce

-- First a table is made concatinating all of the reviews
DROP VIEW IF EXISTS concat_reviews;
CREATE VIEW concat_reviews AS
	SELECT hw_number, sentiment_score, 'sp2017' as year
        FROM sp2017_predictions
        UNION ALL
        SELECT hw_number, sentiment_score, 'sp2018' as year
        FROM sp2018_predictions
        UNION ALL
        SELECT hw_number, sentiment_score, 'fl2016' as year
        FROM fl2016_predictions
        UNION ALL
        SELECT hw_number, sentiment_score, 'fl2017' as year
	FROM fl2017_predictions
	UNION ALL
	SELECT hw_number, sentiment_score, 'fl2018' as year
	FROM fl2018_predictions;

-- Then the data points are labeled as theory or implementation depending on the hw number	
DROP VIEW IF EXISTS theory_or_imp_label;
CREATE VIEW theory_or_imp_label AS
        SELECT CASE WHEN '1, 2, 3' rlike hw_number then 'theory'
	ELSE 'implementation' END AS type,
	sentiment_score
        FROM concat_reviews;

-- Then the average sentiment is computed for each new type label
DROP TABLE IF EXISTS theory_or_imp;
CREATE TABLE theory_or_imp AS
	SELECT type, AVG(sentiment_score) AS sentiment_score
	FROM theory_or_imp_label
	GROUP BY type;

DROP VIEW theory_or_imp_label;

-- Now we label them by a type, spark or mapreduce, based on hw number
DROP VIEW IF EXISTS type_label;
CREATE VIEW type_label AS
        SELECT CASE WHEN '4, 5, 6' rlike hw_number then 'MapReduce'
        WHEN '8, 9' rlike hw_number then 'SPARK' END AS type,
        sentiment_score
        FROM concat_reviews;

-- Then the average sentiment is computed for each new tool label
DROP TABLE IF EXISTS type_sent;
CREATE TABLE type_sent AS
        SELECT type, AVG(sentiment_score) AS sentiment_score
        FROM type_label
        GROUP BY type;

DROP VIEW type_label;
DROP VIEW concat_review;
