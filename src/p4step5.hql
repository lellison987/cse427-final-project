-- Checks to see how "helpful" each review was, aka check to see
-- how much of the total words are sentiment words (pos or neg)

-- FL2016

-- Break up reviews into individual words
DROP TABLE splitWords;
CREATE TABLE splitWords
        (hw_id STRING,
         hw_number INT,
         label STRING,
         word STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE splitWords
SELECT hw_id, hw_number, label, word
        FROM fl2016 
        LATERAL VIEW explode(split(review, ' ')) splitWords as word;

-- Identify if words have sentiment value
DROP TABLE word_sents;
CREATE TABLE word_sents
        (hw_id STRING,
         hw_number INT,
         label STRING,
         word STRING,
	 sentiment STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE  word_sents
SELECT r.hw_id, r.hw_number, r.label, r.word, sentiment_words.sentiment AS sentiment
        FROM splitWords r
        LEFT OUTER JOIN sentiment_words
        ON (r.word = sentiment_words.word);

-- Count number of positive and negative words per review, and total words
DROP TABLE sent_counts;
CREATE TABLE sent_counts
        (hw_id STRING,
         hw_number INT,
         label STRING,
         numWords INT,
         numPos INT,
	 numNeg INT
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE sent_counts
SELECT hw_id, hw_number, label, COUNT(*) as numWords, COUNT(case sentiment when 'positive' then 1 else null end) AS numPos, COUNT(CASE sentiment WHEN 'negative' THEN 1 ELSE NULL end) AS numNeg
        FROM word_sents
        GROUP BY hw_id, hw_number, label;

-- Calculate the 'helpfulness' score
DROP TABLE fl2016_scores;
CREATE TABLE fl2016_scores
        (hw_id STRING,
         hw_number INT,
         label STRING,
         helpfulness_score DOUBLE
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE fl2016_scores
SELECT hw_id, hw_number, label, (numpos + numneg)/(numwords) AS helpfulness_score
        FROM sent_counts;

-- SP2017

-- Break up reviews into individual words
DROP TABLE splitWords;
CREATE TABLE splitWords
        (hw_id STRING,
         hw_number INT,
         label STRING,
         word STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE splitWords
SELECT hw_id, hw_number, label, word
        FROM sp2017
        LATERAL VIEW explode(split(review, ' ')) splitWords as word;

-- Identify if words have sentiment value
DROP TABLE word_sents;
CREATE TABLE word_sents
        (hw_id STRING,
         hw_number INT,
         label STRING,
         word STRING,
         sentiment STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE  word_sents
SELECT r.hw_id, r.hw_number, r.label, r.word, sentiment_words.sentiment AS sentiment
        FROM splitWords r
        LEFT OUTER JOIN sentiment_words
        ON (r.word = sentiment_words.word);

-- Count number of positive and negative words per review, and total words
DROP TABLE sent_counts;
CREATE TABLE sent_counts
        (hw_id STRING,
         hw_number INT,
         label STRING,
         numWords INT,
         numPos INT,
         numNeg INT
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE sent_counts
SELECT hw_id, hw_number, label, COUNT(*) as numWords, COUNT(case sentiment when 'positive' then 1 else null end) AS numPos, COUNT(CASE sentiment WHEN 'negative' THEN 1 ELSE NULL end) AS numNeg
        FROM word_sents
        GROUP BY hw_id, hw_number, label;

-- Calculate the 'helpfulness' score
DROP TABLE sp2017_scores;
CREATE TABLE sp2017_scores
        (hw_id STRING,
         hw_number INT,
         label STRING,
         helpfulness_score DOUBLE
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE sp2017_scores
SELECT hw_id, hw_number, label, (numpos + numneg)/(numwords) AS helpfulness_score
        FROM sent_counts;

-- FL2017

-- Break up reviews into individual words
DROP TABLE splitWords;
CREATE TABLE splitWords
        (hw_id STRING,
         hw_number INT,
         label STRING,
         word STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE splitWords
SELECT hw_id, hw_number, label, word
        FROM fl2017
        LATERAL VIEW explode(split(review, ' ')) splitWords as word;

-- Identify if words have sentiment value
DROP TABLE word_sents;
CREATE TABLE word_sents
	(hw_id STRING,
         hw_number INT,
         label STRING,
         word STRING,
         sentiment STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE  word_sents
SELECT r.hw_id, r.hw_number, r.label, r.word, sentiment_words.sentiment AS sentiment
        FROM splitWords r
        LEFT OUTER JOIN sentiment_words
        ON (r.word = sentiment_words.word);

-- Count number of positive and negative words per review, and total words
DROP TABLE sent_counts;
CREATE TABLE sent_counts
        (hw_id STRING,
         hw_number INT,
         label STRING,
         numWords INT,
         numPos INT,
         numNeg INT
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE sent_counts
SELECT hw_id, hw_number, label, COUNT(*) as numWords, COUNT(case sentiment when 'positive' then 1 else null end) AS numPos, COUNT(CASE sentiment WHEN 'negative' THEN 1 ELSE NULL end) AS numNeg
        FROM word_sents
        GROUP BY hw_id, hw_number, label;

-- Calculate the 'helpfulness' score
DROP TABLE fl2017_scores;
CREATE TABLE fl2017_scores
        (hw_id STRING,
         hw_number INT,
         label STRING,
         helpfulness_score DOUBLE
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE fl2017_scores
SELECT hw_id, hw_number, label, (numpos + numneg)/(numwords) AS helpfulness_score
        FROM sent_counts;

-- SP2018

-- Break up reviews into individual words
DROP TABLE splitWords;
CREATE TABLE splitWords
        (hw_id STRING,
         hw_number INT,
         label STRING,
         word STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE splitWords
SELECT hw_id, hw_number, label, word
        FROM sp2018
        LATERAL VIEW explode(split(review, ' ')) splitWords as word;

-- Identify if words have sentiment value
DROP TABLE word_sents;
CREATE TABLE word_sents
        (hw_id STRING,
         hw_number INT,
         label STRING,
         word STRING,
         sentiment STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE  word_sents
SELECT r.hw_id, r.hw_number, r.label, r.word, sentiment_words.sentiment AS sentiment
        FROM splitWords r
        LEFT OUTER JOIN sentiment_words
        ON (r.word = sentiment_words.word);

-- Count number of positive and negative words per review, and total words
DROP TABLE sent_counts;
CREATE TABLE sent_counts
        (hw_id STRING,
         hw_number INT,
         label STRING,
         numWords INT,
         numPos INT,
         numNeg INT
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE sent_counts
SELECT hw_id, hw_number, label, COUNT(*) as numWords, COUNT(case sentiment when 'positive' then 1 else null end) AS numPos, COUNT(CASE sentiment WHEN 'negative' THEN 1 ELSE NULL end) AS numNeg
        FROM word_sents
        GROUP BY hw_id, hw_number, label;

-- Calculate the 'helpfulness' score
DROP TABLE sp2018_scores;
CREATE TABLE sp2018_scores
        (hw_id STRING,
         hw_number INT,
         label STRING,
         helpfulness_score DOUBLE
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE sp2018_scores
SELECT hw_id, hw_number, label, (numpos + numneg)/(numwords) AS helpfulness_score
        FROM sent_counts;

-- FL2018

-- Break up reviews into individual words
DROP TABLE splitWords;
CREATE TABLE splitWords
        (hw_id STRING,
         hw_number INT,
         label STRING,
         word STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE splitWords
SELECT hw_id, hw_number, label, word
        FROM fl2018
        LATERAL VIEW explode(split(review, ' ')) splitWords as word;

-- Identify if words have sentiment value
DROP TABLE word_sents;
CREATE TABLE word_sents
        (hw_id STRING,
         hw_number INT,
         label STRING,
         word STRING,
         sentiment STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE  word_sents
SELECT r.hw_id, r.hw_number, r.label, r.word, sentiment_words.sentiment AS sentiment
        FROM splitWords r
        LEFT OUTER JOIN sentiment_words
        ON (r.word = sentiment_words.word);

-- Count number of positive and negative words per review, and total words
DROP TABLE sent_counts;
CREATE TABLE sent_counts
        (hw_id STRING,
         hw_number INT,
         label STRING,
         numWords INT,
         numPos INT,
         numNeg INT
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE sent_counts
SELECT hw_id, hw_number, label, COUNT(*) as numWords, COUNT(case sentiment when 'positive' then 1 else null end) AS numPos, COUNT(CASE sentiment WHEN 'negative' THEN 1 ELSE NULL end) AS numNeg
        FROM word_sents
        GROUP BY hw_id, hw_number, label;

-- Calculate the 'helpfulness' score
DROP TABLE fl2018_scores;
CREATE TABLE fl2018_scores
        (hw_id STRING,
         hw_number INT,
         label STRING,
         helpfulness_score DOUBLE
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
INSERT OVERWRITE TABLE fl2018_scores
SELECT hw_id, hw_number, label, (numpos + numneg)/(numwords) AS helpfulness_score
        FROM sent_counts;


-- Look at avg. helpfulness scores across semesters

-- Command for finding avg. helpfulness score for fl2016
-- avg. was 0.1793746720844242 
--SELECT AVG(helpfulness_score) 
--      FROM fl2016_scores;

-- Command for finding avg. helpfulness score for sp2017
-- avg. was 0.16209215842524355
--SELECT AVG(helpfulness_score)
--      FROM sp2017_scores;

-- Command for finding avg. helpfulness score for fl2017
-- avg. was 0.15760218799015188
--SELECT AVG(helpfulness_score)
--      FROM fl2017_scores;

-- Command for finding avg. helpfulness score for sp2018
-- avg. was 0.1794911405603384
--SELECT AVG(helpfulness_score)
--      FROM sp2018_scores;

-- Command for finding avg. helpfulness score for fl2018
-- avg. was 0.15950008222888676
--SELECT AVG(helpfulness_score)
--      FROM fl2018_scores;

-- Compare avg. helpfulness scores for different labels

-- FL2016
DROP TABLE fl16compare;
CREATE TABLE fl16compare AS
        SELECT label, AVG(helpfulness_score) AS score
        FROM fl2016_scores
        GROUP BY label;

-- SP2017
DROP TABLE sp17compare;
CREATE TABLE sp17compare AS
        SELECT label, AVG(helpfulness_score) AS score
        FROM sp2017_scores
        GROUP BY label;

-- FL2017
DROP TABLE fl17compare;
CREATE TABLE fl17compare AS
        SELECT label, AVG(helpfulness_score) AS score
        FROM fl2017_scores
        GROUP BY label;

-- SP2018
DROP TABLE sp18compare;
CREATE TABLE sp18compare AS
        SELECT label, AVG(helpfulness_score) AS score
        FROM sp2018_scores
        GROUP BY label;

-- FL2018
DROP TABLE fl18compare;
CREATE TABLE fl18compare AS
        SELECT label, AVG(helpfulness_score) AS score
        FROM fl2018_scores
        GROUP BY label;

