-- Computes the overall sentiment averages for each semester

-- First compute the average total sentiment score for each year and then concatinate this into one table
-- sp 17
DROP VIEW IF EXISTS sp17total;
CREATE VIEW sp17total AS
        SELECT 'sp2017' AS year, 
	AVG(sentiment_score) as sentiment_score
        FROM sp2017_predictions;

-- sp 18
DROP VIEW IF EXISTS sp18total;
CREATE VIEW sp18total AS
        SELECT 'sp2018' AS year, 
	AVG(sentiment_score) as sentiment_score
        FROM sp2018_predictions;

-- fl 16
DROP VIEW IF EXISTS fl16total;
CREATE VIEW fl16total AS
        SELECT 'fl2016' AS year, 
	AVG(sentiment_score) as sentiment_score
        FROM fl2016_predictions;

-- fl 17
DROP VIEW IF EXISTS fl17total;
CREATE VIEW fl17total AS
	SELECT 'fl2017' AS year, AVG(sentiment_score) as sentiment_score
	FROM fl2017_predictions;

-- fl 18
DROP VIEW IF EXISTS fl18total;
CREATE VIEW fl18total AS
        SELECT 'fl2018' AS year, 
	AVG(sentiment_score) as sentiment_score
        FROM fl2018_predictions;

-- total table (concatinating all results)
DROP TABLE IF EXISTS totalsent;
CREATE TABLE totalsent AS
	SELECT year, sentiment_score
	FROM sp17total
	UNION ALL
	SELECT year, sentiment_score
        FROM sp18total
	UNION ALL
        SELECT year, sentiment_score
        FROM fl16total
	UNION ALL
        SELECT year, sentiment_score
        FROM fl17total
	UNION ALL
        SELECT year, sentiment_score
        FROM fl18total;

DROP VIEW fl2016total;
DROP VIEW sp2017total;
DROP VIEW sp2018total;
DROP VIEW fl2017total;
DROP VIEW fl2018total;







