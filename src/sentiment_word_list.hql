-- script to create the list of sentiment words, with negation handling.
-- make sure pos-words.txt and neg-words.txt are in the final-project/text/ folder.

-- compile positive and negative words into hive tables
-- alg: load positive words and negated negative words, and a simple table just with value 'positive'.  cross join them to get schema (word, 'positive').
--              Do the same with negative words.
--              concat the two tables to get one list of sentiment words.

DROP TABLE IF EXISTS pos_words;
CREATE TABLE pos_words (word STRING);
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/pos-words.txt'
        OVERWRITE INTO TABLE pos_words;
DROP TABLE IF EXISTS neg_words;
CREATE TABLE neg_words (word STRING);
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/neg-words.txt'
        OVERWRITE INTO TABLE neg_words;

-- append negated words from the opposite list into each table.
CREATE TABLE intermediate AS
SELECT CONCAT('!', word) AS word
        FROM neg_words;
INSERT INTO TABLE neg_words
        SELECT CONCAT('!', word) AS word
        FROM pos_words;
INSERT INTO TABLE pos_words
        SELECT word
        FROM intermediate;
DROP TABLE intermediate;

-- append column sentiment to each word, based on if it's positive or negative.
CREATE TABLE positive (sentiment STRING);
INSERT OVERWRITE TABLE positive values('positive');
CREATE VIEW pos_sentiments AS
        SELECT * FROM pos_words
        CROSS JOIN positive;

CREATE TABLE negative (sentiment STRING);
INSERT OVERWRITE TABLE negative values('negative');
CREATE VIEW neg_sentiments AS
        SELECT * FROM neg_words
        CROSS JOIN negative;

DROP TABLE IF EXISTS sentiment_words;
CREATE TABLE sentiment_words AS
        SELECT * FROM pos_sentiments
        UNION ALL
        SELECT * FROM neg_sentiments;

DROP VIEW pos_sentiments;
DROP VIEW neg_sentiments;
DROP TABLE positive;
DROP TABLE negative;
DROP TABLE pos_words;
DROP TABLE neg_words;
