# preprocessing using milestone2/problem2/problem2_script.hql
First, copy the review folders and updated_stopwords.txt into the final_project/text/ folder in the svn repository. then execute the script in cmd line via "hive -f problem2_script"

# sentiment_word_list.hql: create the table of sentiment words, with negation handing
* copy pos-words.txt and neg-words.txt into /final_project/text/ before executing the script.
* look at comments in script for details about negation handling.
* make sure you run this before the following scripts.

# basic_predictor.hql: predict the sentiments of the positive and negative reviews of every semester, as a different table.
### setup
* Run the script, and you will get resulting tables per semester of the actual and predicted labels.
* for further analysis required in problem 4, you should be able to query these tables to get answers.

# p4step1b: analyze 2-grams and 3-grams.
### setup
* ensure the sentiment_words table already exists.
* copy allReviews into /final_project/text.
* use the following command to get the n-grams from all reviews:
  * hive -f p4step1b.hql --hiveconf path=~/cse427s/final_project/text/allReviews --hiveconf n=<2 or 3, whichever n-gram you're looking for>



