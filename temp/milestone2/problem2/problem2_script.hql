-- Hive script for Project 2 Step 2: Test Pre-processing
-- (milestone 2)

-- Four separate tables will be created to keep the review
-- data separate for each of the four semesters we are
-- studying: FL2016, SP2017, FL2017, SP2018. The FL2018
-- data has not yet been uploaded so it is not included.
-- First drop the tables in case they exist from a previous
-- query, then create the tables specifying format.
DROP TABLE fl2016;
DROP TABLE sp2017;
DROP TABLE fl2017;
DROP TABLE sp2018;
DROP TABLE fl2018;
CREATE TABLE fl2016
        (hw_id STRING,
         hw_number INT,
         label STRING,
         review STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
CREATE TABLE sp2017
        (hw_id STRING,
         hw_number INT,
         label STRING,
         review STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
CREATE TABLE fl2017
        (hw_id STRING,
         hw_number INT,
         label STRING,
         review STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
CREATE TABLE sp2018
        (hw_id STRING,
         hw_number INT,
         label STRING,
         review STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
CREATE TABLE fl2018
        (hw_id STRING,
         hw_number INT,
         label STRING,
         review STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
-- Load all data into their respective tables
-- Load Fall 2016 data:
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_FL2016/negative' OVERWRITE INTO TABLE fl2016;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_FL2016/neutral' INTO TABLE fl2016;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_FL2016/not_labled' INTO TABLE fl2016;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_FL2016/positive' INTO TABLE fl2016;
-- Load Spring 2017 data:
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_SP2017/negative' OVERWRITE INTO TABLE sp2017;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_SP2017/neutral' INTO TABLE sp2017;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_SP2017/not_labled' INTO TABLE sp2017;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_SP2017/positive' INTO TABLE sp2017;
-- Load Fall 2017 data:
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_FL2017/negative' OVERWRITE INTO TABLE fl2017;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_FL2017/neutral' INTO TABLE fl2017;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_FL2017/not_labled' INTO TABLE fl2017;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_FL2017/positive' INTO TABLE fl2017;
-- Load Spring 2018 data:
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_SP2018/negative' OVERWRITE INTO TABLE sp2018;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_SP2018/neutral' INTO TABLE sp2018;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_SP2018/not_labled' INTO TABLE sp2018;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_SP2018/positive' INTO TABLE sp2018;
-- Load Fall 2018 data:
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_FL2018/negative' OVERWRITE INTO TABLE fl2018;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_FL2018/neutral' INTO TABLE fl2018;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_FL2018/not_labled' INTO TABLE fl2018;
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/hw_reviews_FL2018/positive' INTO TABLE fl2018;
-- Create a table for the stop words
DROP TABLE IF EXISTS stop_words;
CREATE TABLE stop_words
        (word STRING)
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
-- Load the stop words data into its table
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/updated_stopwords' OVERWRITE INTO TABLE stop_words;

-- Pre-process text data into all lower-case letters
-- and remove the stop words
-- For Fall 2016 data:
INSERT OVERWRITE TABLE fl2016
SELECT d.hw_id, d.hw_number, d.label, CONCAT_WS(" ", COLLECT_LIST(d.word)) AS
 words
        FROM (SELECT c.hw_id, c.hw_number, c.label, c.word
                FROM (SELECT b.hw_id, b.hw_number, b.label, expl.word
                        FROM (SELECT a.hw_id, a.hw_number, a.label, exp.words
                                FROM fl2016 a
                                LATERAL VIEW EXPLODE(SENTENCES(LOWER(a.review))) exp AS words) b
                        LATERAL VIEW EXPLODE(b.words) expl AS word) c
                LEFT OUTER JOIN stop_words s
                ON (c.word = s.word)
                WHERE s.word IS NULL) d
        GROUP BY d.hw_id, d.hw_number, d.label;
-- For Spring 2017 data:
INSERT OVERWRITE TABLE sp2017
SELECT d.hw_id, d.hw_number, d.label, CONCAT_WS(" ", COLLECT_LIST(d.word)) AS
 words
        FROM (SELECT c.hw_id, c.hw_number, c.label, c.word
                FROM (SELECT b.hw_id, b.hw_number, b.label, expl.word
                        FROM (SELECT a.hw_id, a.hw_number, a.label, exp.words
                                FROM sp2017 a
                                LATERAL VIEW EXPLODE(SENTENCES(LOWER(a.review))) exp AS words) b
                        LATERAL VIEW EXPLODE(b.words) expl AS word) c
                LEFT OUTER JOIN stop_words s
                ON (c.word = s.word)
                WHERE s.word IS NULL) d
        GROUP BY d.hw_id, d.hw_number, d.label;
-- For Fall 2017 data:
INSERT OVERWRITE TABLE fl2017
SELECT d.hw_id, d.hw_number, d.label, CONCAT_WS(" ", COLLECT_LIST(d.word)) AS
 words
        FROM (SELECT c.hw_id, c.hw_number, c.label, c.word
                FROM (SELECT b.hw_id, b.hw_number, b.label, expl.word
                        FROM (SELECT a.hw_id, a.hw_number, a.label, exp.words
                                FROM fl2017 a
                                LATERAL VIEW EXPLODE(SENTENCES(LOWER(a.review))) exp AS words) b
                        LATERAL VIEW EXPLODE(b.words) expl AS word) c
                LEFT OUTER JOIN stop_words s
                ON (c.word = s.word)
                WHERE s.word IS NULL) d
        GROUP BY d.hw_id, d.hw_number, d.label;
-- For Spring 2018 data:
INSERT OVERWRITE TABLE sp2018
SELECT d.hw_id, d.hw_number, d.label, CONCAT_WS(" ", COLLECT_LIST(d.word)) AS
 words
        FROM (SELECT c.hw_id, c.hw_number, c.label, c.word
                FROM (SELECT b.hw_id, b.hw_number, b.label, expl.word
                        FROM (SELECT a.hw_id, a.hw_number, a.label, exp.words
                                FROM sp2018 a
                                LATERAL VIEW EXPLODE(SENTENCES(LOWER(a.review))) exp AS words) b
                        LATERAL VIEW EXPLODE(b.words) expl AS word) c
                LEFT OUTER JOIN stop_words s
                ON (c.word = s.word)
                WHERE s.word IS NULL) d
        GROUP BY d.hw_id, d.hw_number, d.label;
-- For Fall 2018 data:
INSERT OVERWRITE TABLE fl2018
SELECT d.hw_id, d.hw_number, d.label, CONCAT_WS(" ", COLLECT_LIST(d.word)) AS
 words
        FROM (SELECT c.hw_id, c.hw_number, c.label, c.word
                FROM (SELECT b.hw_id, b.hw_number, b.label, expl.word
                        FROM (SELECT a.hw_id, a.hw_number, a.label, exp.words
                                FROM fl2018 a
                                LATERAL VIEW EXPLODE(SENTENCES(LOWER(a.review))) exp AS words) b
                        LATERAL VIEW EXPLODE(b.words) expl AS word) c
                LEFT OUTER JOIN stop_words s
                ON (c.word = s.word)
                WHERE s.word IS NULL) d
        GROUP BY d.hw_id, d.hw_number, d.label;
