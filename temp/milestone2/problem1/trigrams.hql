-- normalizes all comments on product to lowercase,
-- breaks them into individual words, and then finds
-- the five most common trigrams
SELECT EXPLODE(NGRAMS(SENTENCES(LOWER(message)), 3, 5))
	AS trigrams
	FROM ratings
	WHERE prod_id = 1274673;
