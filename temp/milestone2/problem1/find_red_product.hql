-- First, we have to create the products
-- table becauase it was not created already.
CREATE TABLE products
	(prod_id INT,
	 brand STRING,
	 name STRING,
	 price INT,
	 cost INT,
	 shipping_wt INT
	)
	ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
-- Then, we must load the product data into
-- the products table
LOAD DATA LOCAL INPATH '/home/cloudera/training_materials/analyst/data/pig_multidata/products/part-m-00000' OVERWRITE INTO TABLE products;
LOAD DATA LOCAL INPATH '/home/cloudera/training_materials/analyst/data/pig_multidata/products/part-m-00001' INTO TABLE products;
LOAD DATA LOCAL INPATH '/home/cloudera/training_materials/analyst/data/pig_multidata/products/part-m-00002' INTO TABLE products;
LOAD DATA LOCAL INPATH '/home/cloudera/training_materials/analyst/data/pig_multidata/products/part-m-00003' INTO TABLE products;

-- Now, we can run a query that will display the
-- record for product ID 1274673 in the table
SELECT *
	FROM products
	WHERE prod_id=1274673;
