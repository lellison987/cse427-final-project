-- finds all distinct comments containing
-- the word "red" that are associated with
-- product ID 1274673
SELECT DISTINCT message
	FROM ratings
	WHERE prod_id = 1274673
		AND message LIKE '%red%';
