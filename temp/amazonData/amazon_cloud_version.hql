-- Hive script for pre-processing Amazon review data
-- (problem 5)

--Set hive.strict.checks.cartesian.product to false by default;


-- A reference to hive-hcatalog-core.jar is required
-- so the reviews can be loaded from JSON formatj
ADD JAR s3://catherinekuhn-test0/hive-hcatalog-core.jar;

-- A table will be created to hold the review data.
-- First, drop the table in case it exists from a 
-- previous query, then create the format
DROP TABLE product_reviews;
CREATE TABLE product_reviews
	(reviewerid STRING,
	 asin STRING,
	 reviewername STRING,
	 helpful ARRAY<INT>,
	 reviewtext STRING,
	 overall INT,
	 summary STRING,
	 unixreviewtime INT,
	 reviewtime STRING
	)
	ROW FORMAT SERDE 'org.apache.hive.hcatalog.data.JsonSerDe'
	STORED AS TEXTFILE;
-- Load the review table into the table
LOAD DATA INPATH 's3://catherinekuhn-test0/input/reviews_Musical_Instruments_5.json' OVERWRITE INTO TABLE product_reviews;

-- Create a table for the stop words
DROP TABLE stop_words;
CREATE TABLE stop_words
        (word STRING)
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
-- Load the stop words data into its table
LOAD DATA INPATH 's3://catherinekuhn-test0/input/updated_stopwords' OVERWRITE INTO TABLE stop_words;

-- Create new table to hold only the information needed from product_reviews,
DROP TABLE long_reviews;
CREATE TABLE long_reviews
	(reviewer_id STRING,
	 product_id STRING,
	 rating STRING,
	 review STRING,
	 count INT
	)
	ROW FORMAT DELIMITED
	FIELDS TERMINATED BY '\t';

-- Remove rows that contain reviews with < 50 words
INSERT OVERWRITE TABLE long_reviews
SELECT reviewerid, asin, overall, reviewtext, SIZE(SPLIT(reviewtext, " ")) 
	FROM product_reviews 
	WHERE SIZE(SPLIT(reviewtext, " ")) > 49;

-- Create new table to hold only the information needed from count_reviews
DROP TABLE reviews;
CREATE TABLE reviews
        (reviewer_id STRING,
         product_id STRING,
         rating STRING,
         review STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';

-- Pre-process review data to remove non-word characters, change to lowercase, and remove all stopwords
INSERT OVERWRITE TABLE reviews
SELECT d.reviewer_id, d.product_id, d.rating, CONCAT_WS(" ", COLLECT_LIST(d.word)) AS
 words
        FROM (SELECT c.reviewer_id, c.product_id, c.rating, c.word
                FROM (SELECT b.reviewer_id, b.product_id, b.rating, expl.word
                        FROM (SELECT a.reviewer_id, a.product_id, a.rating, exp.words
                                FROM long_reviews a
                                LATERAL VIEW EXPLODE(SENTENCES(LOWER(REGEXP_REPLACE(REGEXP_REPLACE(a.review, '\'', ''), '[^A-Za-z0-9 ]', ' ')))) exp AS words) b
                        LATERAL VIEW EXPLODE(b.words) expl AS word) c
                LEFT OUTER JOIN stop_words s
                ON (c.word = s.word)
                WHERE s.word IS NULL) d
        GROUP BY d.reviewer_id, d.product_id, d.rating;

--INSERT OVERWRITE LOCAL DIRECTORY '/home/cloudera/cse427s/final_project/text/pre_amazon/reviews/' SELECT * FROM reviews;

DROP TABLE product_reviews;
DROP TABLE long_reviews;

--creating the positive and negative lists
DROP TABLE IF EXISTS pos_words;
CREATE TABLE pos_words (word STRING);
LOAD DATA INPATH 's3://catherinekuhn-test0/input/pos-words.txt'
        OVERWRITE INTO TABLE pos_words;
DROP TABLE IF EXISTS neg_words;
CREATE TABLE neg_words (word STRING);
LOAD DATA INPATH 's3://catherinekuhn-test0/input/neg-words.txt'
        OVERWRITE INTO TABLE neg_words;

-- append negated words from the opposite list into each table.
CREATE TABLE intermediate AS
SELECT CONCAT('!', word) AS word
        FROM neg_words;
INSERT INTO TABLE neg_words
        SELECT CONCAT('!', word) AS word
        FROM pos_words;
INSERT INTO TABLE pos_words
        SELECT word
        FROM intermediate;
DROP TABLE intermediate;

-- append column sentiment to each word, based on if it's positive or negative.
DROP TABLE IF EXISTS positive;
CREATE TABLE positive (sentiment STRING);
INSERT OVERWRITE TABLE positive values('positive');
CREATE VIEW pos_sentiments AS
        SELECT * FROM pos_words
        CROSS JOIN positive;

DROP TABLE IF EXISTS negative;
CREATE TABLE negative (sentiment STRING);
INSERT OVERWRITE TABLE negative values('negative');
CREATE VIEW neg_sentiments AS
        SELECT * FROM neg_words
        CROSS JOIN negative;

DROP TABLE IF EXISTS sentiment_words;
CREATE TABLE sentiment_words AS
        SELECT * FROM pos_sentiments
        UNION ALL
        SELECT * FROM neg_sentiments;

DROP VIEW pos_sentiments;
DROP VIEW neg_sentiments;
DROP TABLE positive;
DROP TABLE negative;
DROP TABLE pos_words;
DROP TABLE neg_words;

--ratings come in format: reviewer_id, product_id, rating, review
--the output of this is in the format reviewer_id, product_id, rating, review, sentiment score, predicted rating

--first filter out ratings with 3 stars, replace all intances of "not" with "!"
DROP VIEW IF EXISTS useful_data;
CREATE VIEW useful_data AS
	SELECT reviewer_id, product_id, rating, REGEXP_REPLACE(review, 'not\\s+', '!') AS review
	FROM reviews
	WHERE rating IN ('1', '2', '4', '5');

--splitting up reviews into words
DROP VIEW IF EXISTS splitWords;
CREATE VIEW splitWords AS
	SELECT reviewer_id, product_id, rating, word
	FROM useful_data
        LATERAL VIEW explode(split(review, ' ')) splitWords AS word;

--selecting words that have sentiment meaning
DROP VIEW IF EXISTS word_sents;
CREATE VIEW word_sents AS
	SELECT r.reviewer_id, r.product_id, r.rating, r.word, sentiment_words.sentiment AS sentiment
	FROM splitWords r
	LEFT OUTER JOIN sentiment_words
	ON (r.word = sentiment_words.word);

--assinging a sentiment value to words
DROP VIEW IF EXISTS sent_counts;
CREATE VIEW sent_counts AS
	SELECT reviewer_id, product_id, rating, COUNT(*) as numWords,
	COUNT(case sentiment when 'positive' then 1 else null end) AS numPos, 
	COUNT(CASE sentiment WHEN 'negative' THEN 1 ELSE NULL end) AS numNeg
	FROM word_sents
	GROUP BY reviewer_id, product_id, rating;

--computing a sentiment score
DROP VIEW IF EXISTS stats;
CREATE VIEW stats AS
	SELECT reviewer_id, product_id, rating, (numPos - numNeg)/(numPos +  numNeg) AS sentiment_score
	FROM sent_counts;

--assinging a rating
DROP TABLE IF EXISTS amazon_predictions;
CREATE TABLE amazon_predictions AS
	SELECT reviewer_id, product_id, rating, sentiment_score,
	CASE WHEN sentiment_score<0 THEN '1, 2' 
	ELSE '4, 5' END AS prediction
	FROM stats;

--finding percentage correct
INSERT OVERWRITE DIRECTORY 's3://catherinekuhn-test0/output/one'
	SELECT COUNT(IF(prediction rlike rating,'correct', NULL))/COUNT(*)
	FROM amazon_predictions;

DROP VIEW useful_data;
DROP VIEW splitWords;

INSERT OVERWRITE DIRECTORY 's3://catherinekuhn-test0/output/two' SELECT * FROM amazon_predictions;


