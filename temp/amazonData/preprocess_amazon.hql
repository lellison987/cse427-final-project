-- Hive script for pre-processing Amazon review data
-- (problem 5)

-- A reference to hive-hcatalog-core.jar is required
-- so the reviews can be loaded from JSON format
ADD JAR /usr/lib/hive-hcatalog/share/hcatalog/hive-hcatalog-core.jar;

-- A table will be created to hold the review data.
-- First, drop the table in case it exists from a 
-- previous query, then create the format
DROP TABLE product_reviews;
CREATE TABLE product_reviews
	(reviewerid STRING,
	 asin STRING,
	 reviewername STRING,
	 helpful ARRAY<INT>,
	 reviewtext STRING,
	 overall INT,
	 summary STRING,
	 unixreviewtime INT,
	 reviewtime STRING
	)
	ROW FORMAT SERDE 'org.apache.hive.hcatalog.data.JsonSerDe'
	STORED AS TEXTFILE;
-- Load the review table into the table
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/pre_amazon/reviews_Musical_Instruments_5.json' OVERWRITE INTO TABLE product_reviews;

-- Create a table for the stop words
DROP TABLE stop_words;
CREATE TABLE stop_words
        (word STRING)
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
-- Load the stop words data into its table
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/updated_stopwords' OVERWRITE INTO TABLE stop_words;

-- Create new table to hold only the information needed from product_reviews,
DROP TABLE long_reviews;
CREATE TABLE long_reviews
	(reviewer_id STRING,
	 product_id STRING,
	 rating INT,
	 review STRING,
	 count INT
	)
	ROW FORMAT DELIMITED
	FIELDS TERMINATED BY '\t';

-- Remove rows that contain reviews with < 50 words
INSERT OVERWRITE TABLE long_reviews
SELECT reviewerid, asin, overall, reviewtext, SIZE(SPLIT(reviewtext, " ")) 
	FROM product_reviews 
	WHERE SIZE(SPLIT(reviewtext, " ")) > 49;

-- Create new table to hold only the information needed from count_reviews
DROP TABLE reviews;
CREATE TABLE reviews
        (reviewer_id STRING,
         product_id STRING,
         rating INT,
         review STRING
        )
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';

-- Pre-process review data to remove non-word characters, change to lowercase, and remove all stopwords
INSERT OVERWRITE TABLE reviews
SELECT d.reviewer_id, d.product_id, d.rating, CONCAT_WS(" ", COLLECT_LIST(d.word)) AS
 words
        FROM (SELECT c.reviewer_id, c.product_id, c.rating, c.word
                FROM (SELECT b.reviewer_id, b.product_id, b.rating, expl.word
                        FROM (SELECT a.reviewer_id, a.product_id, a.rating, exp.words
                                FROM long_reviews a
                                LATERAL VIEW EXPLODE(SENTENCES(LOWER(REGEXP_REPLACE(REGEXP_REPLACE(a.review, '\'', ''), '[^A-Za-z0-9 ]', ' ')))) exp AS words) b
                        LATERAL VIEW EXPLODE(b.words) expl AS word) c
                LEFT OUTER JOIN stop_words s
                ON (c.word = s.word)
                WHERE s.word IS NULL) d
        GROUP BY d.reviewer_id, d.product_id, d.rating;

INSERT OVERWRITE LOCAL DIRECTORY '/home/cloudera/cse427s/final_project/text/pre_amazon/reviews/' SELECT * FROM reviews;

DROP TABLE product_reviews;
DROP TABLE long_reviews;
