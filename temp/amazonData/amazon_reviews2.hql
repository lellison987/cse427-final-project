--ratings come in format: reviewer_id, product_id, rating, review
--the output of this is in the format reviewer_id, product_id, rating, review, sentiment score, predicted rating

--first filter out ratings with 3 stars, replace all intances of "not" with "!"
DROP VIEW IF EXISTS useful_data;
CREATE VIEW useful_data AS
	SELECT reviewer_id, product_id, rating, REGEXP_REPLACE(review, 'not\\s+', '!') AS review
	FROM reviews;

--splitting up reviews into words
DROP VIEW IF EXISTS splitWords;
CREATE VIEW splitWords AS
	SELECT reviewer_id, product_id, rating, word
	FROM useful_data
        LATERAL VIEW explode(split(review, ' ')) splitWords AS word;

--selecting words that have sentiment meaning
DROP VIEW IF EXISTS word_sents;
CREATE VIEW word_sents AS
	SELECT r.reviewer_id, r.product_id, r.rating, r.word, sentiment_words.sentiment AS sentiment
	FROM splitWords r
	LEFT OUTER JOIN sentiment_words
	ON (r.word = sentiment_words.word);

--assinging a sentiment value to words
DROP VIEW IF EXISTS sent_counts;
CREATE VIEW sent_counts AS
	SELECT reviewer_id, product_id, rating, COUNT(*) as numWords,
	COUNT(case sentiment when 'positive' then 1 else null end) AS numPos, 
	COUNT(CASE sentiment WHEN 'negative' THEN 1 ELSE NULL end) AS numNeg
	FROM word_sents
	GROUP BY reviewer_id, product_id, rating;

--computing a sentiment score
DROP VIEW IF EXISTS stats;
CREATE VIEW stats AS
	SELECT reviewer_id, product_id, rating, (numPos - numNeg)/(numPos +  numNeg) AS sentiment_score
	FROM sent_counts;

--assinging a rating
DROP TABLE IF EXISTS amazon_predictions2;
CREATE TABLE amazon_predictions2 AS
	SELECT reviewer_id, product_id, rating, sentiment_score,
	CASE WHEN sentiment_score < -0.1 THEN '1, 2' 
	WHEN sentiment_score > 0.1 THEN '4, 5' 
	ELSE '3' END AS prediction
	FROM stats;

--finding percentage correct
SELECT COUNT(IF(prediction rlike rating,'correct', NULL))/COUNT(*)
	FROM amazon_predictions;


 
DROP VIEW useful_data;
DROP VIEW splitWords;
DROP VIEW word_sents;
DROP VIEW sent_counts;
DROP VIEW stats;
