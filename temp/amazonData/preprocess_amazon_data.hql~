-- Hive script for pre-processing Amazon review data
-- (problem 5)

-- A reference to hive-hcatalog-core.jar is required
-- so the reviews can be loaded from JSON format
ADD JAR s3://catherinekuhn-test0/hive-hcatalog-core.jar;

-- A table will be created to hold the review data.
-- First, drop the table in case it exists from a 
-- previous query, then create the format
DROP TABLE product_reviews;
CREATE TABLE product_reviews
	(reviewerid STRING,
	 asin STRING,
	 reviewername STRING,
	 helpful ARRAY<INT>,
	 reviewtext STRING,
	 overall INT,
	 summary STRING,
	 unixreviewtime INT,
	 reviewtime STRING
	)
	ROW FORMAT SERDE 'org.apache.hive.hcatalog.data.JsonSerDe'
	STORED AS TEXTFILE;
-- Load the review table into the table
LOAD DATA INPATH 's3://catherinekuhn-test0/input/reviews_Musical_Instruments_5.json' OVERWRITE INTO TABLE product_reviews;

-- Create a table for the stop words
DROP TABLE stop_words;
CREATE TABLE stop_words
        (word STRING)
        ROW FORMAT DELIMITED
        FIELDS TERMINATED BY '\t';
-- Load the stop words data into its table
LOAD DATA INPATH 's3://catherinekuhn-test0/input/updated_stopwords' OVERWRITE INTO TABLE stop_words;

-- Create new table to hold only the information needed from product_reviews
DROP TABLE reviews;
CREATE TABLE reviews
	(reviewer_id STRING,
	 product_id STRING,
	 rating INT,
	 review STRING
	)
	ROW FORMAT DELIMITED
	FIELDS TERMINATED BY '\t';

-- Pre-process review data to remove non-word characters, change to lowercase, and remove all stopwords
INSERT OVERWRITE TABLE reviews
SELECT d.reviewerid, d.asin, d.overall, CONCAT_WS(" ", COLLECT_LIST(d.word)) AS
 words
        FROM (SELECT c.reviewerid, c.asin, c.overall, c.word
                FROM (SELECT b.reviewerid, b.asin, b.overall, expl.word
                        FROM (SELECT a.reviewerid, a.asin, a.overall, exp.words
                                FROM product_reviews a
                                LATERAL VIEW EXPLODE(SENTENCES(LOWER(REGEXP_REPLACE(REGEXP_REPLACE(a.reviewtext, '\'', ''), '[^A-Za-z0-9 ]', ' ')))) exp AS words) b
                        LATERAL VIEW EXPLODE(b.words) expl AS word) c
                LEFT OUTER JOIN stop_words s
                ON (c.word = s.word)
                WHERE s.word IS NULL) d
        GROUP BY d.reviewerid, d.asin, d.overall;

INSERT OVERWRITE DIRECTORY 's3://catherinekuhn-test0/output' SELECT * FROM reviews;

DROP TABLE product_reviews;

