-- script to create the list of sentiment words, with negation handling.
-- make sure pos-words.txt and neg-words.txt are in the final-project/text/ folder.

-- compile positive and negative words into hive tables
-- alg: load positive words and negated negative words, and a simple table just with value 'positive'.  cross join them to get schema (word, 'positive').
--              Do the same with negative words.
--              concat the two tables to get one list of sentiment words.

DROP TABLE IF EXISTS pos_words;
CREATE TABLE pos_words (word STRING);
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/pos-words.txt'
        OVERWRITE INTO TABLE pos_words;
DROP TABLE IF EXISTS neg_words;
CREATE TABLE neg_words (word STRING);
LOAD DATA LOCAL INPATH '/home/cloudera/cse427s/final_project/text/neg-words.txt'
        OVERWRITE INTO TABLE neg_words;

-- append negated words from the opposite list into each table.
CREATE TABLE intermediate AS
SELECT CONCAT('!', word) AS word
        FROM neg_words;
INSERT INTO TABLE neg_words
        SELECT CONCAT('!', word) AS word
        FROM pos_words;
INSERT INTO TABLE pos_words
        SELECT word
        FROM intermediate;
DROP TABLE intermediate;

-- append column sentiment to each word, based on if it's positive or negative.
DROP TABLE IF EXISTS pos_sentiments;
CREATE TABLE pos_sentiments AS
	SELECT word, 'positive' AS sentiment
	FROM pos_words; 

DROP TABLE IF EXISTS neg_sentiments;
CREATE TABLE neg_sentiments AS
        SELECT word, 'negative' AS sentiment
	FROM neg_words;


DROP TABLE IF EXISTS sentiment_words;
CREATE TABLE sentiment_words AS
        SELECT * FROM pos_sentiments
        UNION ALL
        SELECT * FROM neg_sentiments;

DROP TABLE pos_sentiments;
DROP TABLE neg_sentiments;
DROP TABLE positive;
DROP TABLE negative;
DROP TABLE pos_words;
DROP TABLE neg_words;



--ratings come in format: reviewer_id, product_id, rating, review
--the output of this is in the format reviewer_id, product_id, rating, review, sentiment score, predicted rating

--first filter out ratings with 3 stars, replace all intances of "not" with "!"
DROP VIEW IF EXISTS useful_data;
CREATE VIEW useful_data AS
	SELECT reviewer_id, product_id, rating, REGEXP_REPLACE(review, 'not\\s+', '!') AS review
	FROM reviews
	WHERE rating IN ('1', '2', '4', '5');

--splitting up reviews into words
DROP VIEW IF EXISTS splitWords;
CREATE VIEW splitWords AS
	SELECT reviewer_id, product_id, rating, word
	FROM useful_data
        LATERAL VIEW explode(split(review, ' ')) splitWords AS word;

--selecting words that have sentiment meaning
DROP VIEW IF EXISTS word_sents;
CREATE VIEW word_sents AS
	SELECT r.reviewer_id, r.product_id, r.rating, r.word, sentiment_words.sentiment AS sentiment
	FROM splitWords r
	LEFT OUTER JOIN sentiment_words
	ON (r.word = sentiment_words.word);

--assinging a sentiment value to words
DROP VIEW IF EXISTS sent_counts;
CREATE VIEW sent_counts AS
	SELECT reviewer_id, product_id, rating, COUNT(*) as numWords,
	COUNT(case sentiment when 'positive' then 1 else null end) AS numPos, 
	COUNT(CASE sentiment WHEN 'negative' THEN 1 ELSE NULL end) AS numNeg
	FROM word_sents
	GROUP BY reviewer_id, product_id, rating;

--computing a sentiment score
DROP VIEW IF EXISTS stats;
CREATE VIEW stats AS
	SELECT reviewer_id, product_id, rating, (numPos - numNeg)/(numPos +  numNeg) AS sentiment_score
	FROM sent_counts;

--assinging a rating
DROP TABLE IF EXISTS amazon_predictions;
CREATE TABLE amazon_predictions AS
	SELECT reviewer_id, product_id, rating, sentiment_score,
	CASE WHEN sentiment_score<0 THEN '1, 2' 
	ELSE '4, 5' END AS prediction
	FROM stats;

--finding percentage correct
SELECT COUNT(IF(prediction rlike rating,'correct', NULL))/COUNT(*)
	FROM amazon_predictions;


 
DROP VIEW useful_data;
DROP VIEW splitWords;
DROP VIEW word_sents;
DROP VIEW sent_counts;
DROP VIEW stats;
