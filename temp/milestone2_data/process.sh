#!/bin/bash
for file in *.txt;
do
        wordcount=$(wc -w < $file | cut -f2);
        if [ $wordcount -lt 50 ];
        then rm $file;
        fi;
        sed -i ':a;N;$!ba;s/\n/ /g' $file;
        sed -i "s/'//g" $file;
        sed -i 's/[^[:alnum:]]/ /g' $file;
        sed -i 's/  */ /g' $file;
        sed -i "s/^/$1\t/" $file;
        hw=${file#hw};
        hw=${hw%%_*};
        id=${file##*_};
        id=${id%.txt};
        sed -i "s/^/$hw\t/" $file;
        sed -i "s/^/$id\t/" $file;
        sed -i -e '$a\' $file;
done